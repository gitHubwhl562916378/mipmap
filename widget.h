#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "mipmaprender.h"

class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    MipMapRender render_;
    QMatrix4x4 pMatrix;
    QVector3D camera_;
};

#endif // WIDGET_H
